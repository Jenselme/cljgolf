(ns cljgolf.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [cljgolf.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[cljgolf started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[cljgolf has shut down successfully]=-"))
   :middleware wrap-dev})
