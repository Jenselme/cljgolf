(ns cljgolf.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[cljgolf started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[cljgolf has shut down successfully]=-"))
   :middleware identity})
