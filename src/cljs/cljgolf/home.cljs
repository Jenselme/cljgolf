(ns cljgolf.home
  (:require [reagent.core :as r]
            [ajax.core :refer [GET]]
            [reitit.core :as reitit]))

(defn competitions-list-display [router competitions]
  (if (empty? competitions)
    [:p "No competitions"]
    [:ul
     (for [item competitions]
       (let [id (:id item)
             label (:label item)
             route (reitit/match-by-name router :competition-details {:id id})
             path (:path route)
             href (str "#" path)]
         ^{:key id}
         [:li [:a {:href href} label]]))]))

; or (clojure.walk/keywordize-keys data)
(defn keywordize-json [data]
  (cond
    (vector? data) (map #(keywordize-json %) data)
    (map? data) (into {}
                      (for [[key value] data]
                        [(keyword key) (keywordize-json value)]))
    :else data
    ))

(defn fetch-next-competitions! [competitions]
  (GET "/api/competitions" {:handler #(reset!
                                        competitions
                                        (:results (keywordize-json %)))}))

(defn next-competitions-list [router]
  (let [competitions (r/atom [])]
    (fetch-next-competitions! competitions)
    (fn []
      [competitions-list-display router @competitions])))

(defn home [router]
  [:div
   [:h1 "Welcome to cljgolf"]
   [:p "This site is here to present golf competitions"]
   [:div
    [:h2 "Next competitions"]
    [next-competitions-list router]]])
