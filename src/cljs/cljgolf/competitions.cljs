(ns cljgolf.competitions
  (:require
    [reitit.core :refer [match-by-path]]
    [reagent.core :as r]
    [ajax.core :refer [GET]]
    [clojure.walk :refer [keywordize-keys]]))

(defn fetch-competition [id competition]
  (GET (str "/api/competitions/" id) {:handler #(reset!
                                                  competition
                                                  (keywordize-keys %))}))

(defn- competition-details [competition]
  (if (nil? competition)
    nil
    (let [ts (js/Date.parse (:date competition))
          js-date (js/Date. ts)
          formatted-date (.toLocaleString js-date)]
      [:<>
       [:h1 (:label competition)]
       [:p formatted-date]
       [:p "In " (:address competition) ", " (:postal_code competition) " " (:city competition)]])))

(defn details [router]
  (let [competition (r/atom nil)
        path (subs (.-hash js/location) 1)
        router-match (match-by-path router path)
        id (:id (:path-params router-match))]
    (fetch-competition id competition)
    (fn []
      [competition-details @competition])))
