(ns cljgolf.counter
  (:require [reagent.core :as r]))

(def total-counts (r/atom 0))

(defn update-total-counts [increment]
  (swap! total-counts + increment))

(defn update-counter [count increment]
  (do
    (swap! count + increment)
    (update-total-counts increment)))

(defn counter
  ([] (counter 1))
  ([increment]
   (let [count (r/atom 0)]
     (fn []
       [:div
        [:p "The count is " @count]
        [:button {:on-click #(update-counter count increment)} "Click me"]]))))

(defn number-10x [number]
  [:p "10x is " (* 10 number)])


(defn update-strange-counter [counter-value number]
  (+ (* 10 number) (* 2 counter-value)))

(defn strange-counter []
  (let [count (r/atom 0)]
    (fn [number]
      [:div
       [:p "Strange count is: " @count " from " number]
       [:button {:on-click #(swap! count update-strange-counter number)} "Strange update"]])))

(defn counters []
  [:div
   [:div "Counter 1: " [counter]]
   [:div "Counter 2: " [counter 2]]
   [:p "Total is: " @total-counts]
   [number-10x @total-counts]
   [strange-counter @total-counts]])
