(ns cljgolf.routes.home
  (:require
    [cljgolf.layout :as layout]
    [cljgolf.db.core :as db]
    [clojure.java.io :as io]
    [cljgolf.middleware :as middleware]
    [ring.util.response]
    [ring.util.http-response :as response]
    [muuntaja.core :refer [encode create]])
  (:import (java.io BufferedReader)))

(def serializer (create))

(defn home-page [request]
  (layout/render request "home.html"))

(defn respond-with-json [data]
  (-> (response/ok (encode serializer "application/json" data))
      (response/header "Content-Type" "application/json; charset=utf-8")))

(defn next-competitions [request]
  (respond-with-json {:results (db/get-next-competitions)}))

(defn clob-to-string [clob-data]
  "From https://stackoverflow.com/a/32159705"
  (with-open [rdr (BufferedReader. (.getCharacterStream clob-data))]
    (apply str (line-seq rdr))))

(defn get-competition-by-id [request]
  (let [id (:id (:path-params request))
        competition (db/get-competition {:id id})
        serialiazable-competition (assoc competition :address
                                                     (clob-to-string (:address competition)))]
    (respond-with-json serialiazable-competition)))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/api/competitions" {:get next-competitions}]
   ["/api/competitions/:id" {:get get-competition-by-id}]
   ["/docs" {:get (fn [_]
                    (-> (response/ok (-> "docs/docs.md" io/resource slurp))
                        (response/header "Content-Type" "text/plain; charset=utf-8")))}]])

