CREATE TABLE competitions_competition (
    id integer NOT NULL PRIMARY KEY AUTO_INCREMENT,
    date datetime NOT NULL,
    label varchar(50) NOT NULL,
    address text NOT NULL,
    postal_code varchar(5) NOT NULL,
    city varchar(100) NOT NULL
);
