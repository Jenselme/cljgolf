-- :name create-competition! :! :n
-- :doc creates a new user record
INSERT INTO competitions_competition
(date, label, address, postal_code, city)
VALUES (:date, :label, :address, :postal_code, :city)

-- :name update-competition! :! :n
-- :doc updates an existing user record
UPDATE competitions_competition
SET date = :date, label = :label
WHERE id = :id

-- :name get-competition :? :1
-- :doc retrieves a user record given the id
SELECT * FROM competitions_competition
WHERE id = :id

-- :name get-all-competitions :? :*
-- :doc get all competitions (limit at 100)
SELECT * FROM competitions_competition

-- :name get-next-competitions :? :*
-- :doc get the 5 next competitions closest of now.
SELECT id, label FROM competitions_competition
ORDER BY date DESC
LIMIT 5
