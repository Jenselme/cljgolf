(ns cljgolf.doo-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [cljgolf.core-test]))

(doo-tests 'cljgolf.core-test)

