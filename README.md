# cljgolf

generated using Luminus version "3.75"

FIXME

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

- To start a web server for the application, run: `lein run`
- To run tests: `lein test`
- To run tests in watch mode: `lein test-refresh`
- To run migrations: `lein migrate`
  - To create a new migration, in a REPL:
  ```clojure
  (start)
  (user/create-migration "add-users-table")
  ```
  - To fill the database:
  ```clojure
  (require '[cljgolf.db.core :as db])
  (dotimes [i 20]
    (db/create-competition! {:date (java.util.Date.)
                              :label (str "Test " i)
                              :address "Ici"
                              :city "Le Mans"
                              :postal_code "72000"}))
  ```
  - To releoad `queries.sql` in the REPL:
  ```clojure
  (require '[cljgolf.db.core :as db])
  (defn setup-queries []
    (if-not (nil? db/*db*)
      (conman/disconnect! db/*db*))
    (conman/bind-connection db/*db* "sql/queries.sql"))
  (setup-queries)
  ```
- To auto build JS: `lein figwheel`
- In a REPL: `lein repl`
  - To start the project: `(start)`

## License

Copyright © 2020 FIXME
